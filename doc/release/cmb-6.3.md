# CMB 6.3 Release Notes
CMB 6.3 is a minor release with new features.  **Note any non-backward compatible changes are in bold italics**. See also [CMB 6.2 Release Notes](cmb-6.2.md).


## CMB 6.3 Dependencies
* CMB 6.3 is based on SMTK 3.3 - For changes made to SMTK 3.3 please see the release notes in SMTK
* CMB 6.3 is based on a ParaView 5.6 variant
* CMB 6.3 is based on Qt 5.9 and supports Qt 5.13.1

## Support for Dark Mode
ModelBuilder can now support MacOS Dark Mode

## Support for Latest Attribute Resource File Formats
ModelBuilder can now read in Attribute Resources using the new 4.0 SBT/SMTK formats and now saves Attribute Resources using 4.0 SMTK files.

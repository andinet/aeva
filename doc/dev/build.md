Building ModelBuilder
=====================

This page describes how to build and install ModelBuilder. It covers building for development, on both Unix-type systems (Linux, HP-UX, Solaris, Mac), and Windows.

At its core, ModelBuilder is a modified version of ParaView designed for extensibility using ParaView's plugin mechanism. As such, the ModelBuilder application only requires ParaView to be *built*, but requires the plugins from the [Simulation and Modeling Toolkit (SMTK)](https://www.computationalmodelbuilder.org/smtk/) to be useful.

Prerequisites
=============
* The ModelBuilder build process requires [CMake](http://www.cmake.org/) version 3.9 or higher and a working compiler. On Unix-like operating systems, it also requires Make or Ninja, while on Windows it requires Visual Studio and Ninja build.
* Building ModelBuilder also requires a build of [ParaView](https://www.paraview.org) >= v5.6 (not a packaged binary). We are working on providing an installable SDK for ParaView; until this is available, we use ParaView's build directory for this purpose.
* For Windows builds, unix-like environments such as Cygwin, or MinGW are not supported.

Download And Install CMake
--------------------------
CMake is a tool that makes cross-platform building simple. On several systems it will probably be already installed. If it is not, please use the following instructions to install it.

There are several precompiled binaries available at the [CMake download page](https://cmake.org/download/).

Add CMake to your PATH environment variable if you downloaded an archive and not an installer.

Download and Build ParaView
---------------------------
Follow the instructions outlined on the [Paraview wiki](http://www.paraview.org/Wiki/ParaView:Build_And_Install) to build ParaView. Version 5.6 is currently supported, though earlier versions of ParaView may also work.

(Recommended) Download and Build SMTK
-------------------------------------
The SMTK project contains plugins that imbue ModelBuilder with the ability to create, manipulate, mesh and annotate models, and to construct and execute simulation workflows. While it is not strictly a required dependency to build ModelBuilder, a version of SMTK provided during ModelBuilder's configuration will automatically make the SMTK plugins available in the built ModelBuilder instance. Follow the instructions outlined in [SMTK's documentation](https://smtk.readthedocs.io/en/latest/index.html) to build SMTK >= v3.0.

Retrieve the Source
-------------------
* [Install Git](git/README.md) -
  Git 1.7.2 or greater is required for development

* [Develop aeva](git/develop.md) - Create a fork and checkout the source code.
    - A useful directory structure is cmb/src for the git source tree, cmb/build for the build tree, and if needed, cmb/install for the install directory.
    - Please follow the instructions in [git/develop.md] for creating a fork and setting up the repository for contributing to aeva.

Run CMake
---------
* `cd cmb/build`
* `cmake-gui ../src` or `ccmake ../src`
* Generator: choose `ninja` for the ninja build system.

Build
-----
* `cd cmb/build`
* `ninja`

ModelBuilder will be in `bin/aeva.exe`

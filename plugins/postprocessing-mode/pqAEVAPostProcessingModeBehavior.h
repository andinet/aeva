//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef plugins_postprocessingmode_pqAEVAPostProcessingModeBehavior_h
#define plugins_postprocessingmode_pqAEVAPostProcessingModeBehavior_h

#include <QActionGroup>

class pqServer;

class pqAEVAPostProcessingModeBehavior : public QActionGroup
{
  Q_OBJECT
  using Superclass = QActionGroup;

public:
  pqAEVAPostProcessingModeBehavior(QObject* parent = nullptr);
  ~pqAEVAPostProcessingModeBehavior() override;

  static pqAEVAPostProcessingModeBehavior* instance();

signals:
  void togglePostProcessingMode(bool enablePostProcessing);

protected slots:
  /**\brief This slot is invoked the first time events are processed after the constructor is run.
    *
    * It must run in the main event loop since pqCoreUtilities::mainWidget() returns
    * nullptr inside the constructor when the plugin is set to autoload.
    */
  virtual void prepare();

  /**\brief This slot is invoked when the post-processing mode button is toggled.
    *
    * It emits togglePostProcessingMode(bool) which can be connected to the main
    * window (when the main widget is of time mbMainWindow).
    */
  virtual void switchModes(QAction* a);

protected:
  class pqInternal;
  pqInternal* m_p;

private:
  Q_DISABLE_COPY(pqAEVAPostProcessingModeBehavior);
};

#endif
